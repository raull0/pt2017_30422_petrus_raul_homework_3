package tables;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import model.Manager;

public class TableManagerDAO extends Tabels<Manager> {
	public Vector<String> getCollumns(Manager t) {
		Vector<String> collumns = new Vector<>();
		for (Field f : t.getClass().getDeclaredFields()) {
			if (!(f.getName().equals("pass")))
				collumns.add(f.getName());
		}

		return collumns;

	}

	public DefaultTableModel buildTableModel(List<Manager> t) {
		model = new DefaultTableModel();
		row = 0;
		boolean d = true;
		collumns = getCollumns(t.get(0));
		for (String s : collumns)
			model.addColumn(s);
		for (Manager t1 : t) {
			Vector<Object> o = new Vector<>();
			for (Field f : t1.getClass().getDeclaredFields()) {
				try {
					if (!(f.getName().equals("pass"))) {
						d = false;
						PropertyDescriptor propertyDescriptor = new PropertyDescriptor(f.getName(), t1.getClass());
						Method method = propertyDescriptor.getReadMethod();
						o.add(method.invoke(t1));
					}
				} catch (Exception e) {
				}
				;

			}
			if (!d) {
				model.insertRow(row, o);
				row++;
			}
		}

		return model;
	}

}
