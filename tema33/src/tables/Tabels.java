package tables;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

public class Tabels<T> {
	protected DefaultTableModel model;
	protected int row;
	protected Vector<String> collumns;
	public DefaultTableModel buildTableModel(List<T> t) {
		model=new DefaultTableModel();
		row=0;
		collumns = getCollumns(t.get(0));
		for(String s : collumns)
			model.addColumn(s);
		for (T t1 : t) {
			Vector<Object> o = new Vector<>();
			for (Field f : t1.getClass().getDeclaredFields()) {
				try {
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(f.getName(), t1.getClass());
					Method method = propertyDescriptor.getReadMethod();
					o.add(method.invoke(t1));
				} catch (Exception e) {
				}
				;

			}
			model.insertRow(row, o);
			row++;
		}
		
		return model;
	}

	public  Vector<String> getCollumns(T t) {
		Vector<String> collumns = new Vector<>();
		for (Field f : t.getClass().getDeclaredFields()) {
			collumns.add(f.getName());
		}

		return collumns;

	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public Vector<String> getCollumns() {
		return collumns;
	}

	public void setCollumns(Vector<String> collumns) {
		this.collumns = collumns;
	}

}
