package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Eroare {
	private JFrame frame ;
	private JTextField eroarea;
	private JPanel panel;
	private Dimension dim;
	public Eroare() {
			dim= Toolkit.getDefaultToolkit().getScreenSize();
			Font font1 = new Font("Verdana", Font.BOLD, 13);
			frame = new JFrame("Error");
			eroarea = new JTextField();
			panel = new JPanel();
			
			frame.setBounds(0, 0, 400, 150);
			frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
			frame.setVisible(true);
			
			panel.setBounds(0, 0, 400, 150);
			panel.setLayout(null);
			panel.setBackground(Color.white);
			frame.add(panel);
			
			eroarea.setBounds(20, 50, 360, 30);
			eroarea.setFont(font1);
			eroarea.setForeground(Color.red);
			eroarea.setBorder(null);
			panel.add(eroarea);
			eroarea.setHorizontalAlignment(JTextField.CENTER);
	}
	public String getEroarea() {
		return eroarea.getText();
	}
	public void setEroarea(String eroarea) {
		this.eroarea.setText(eroarea);
	}

}
