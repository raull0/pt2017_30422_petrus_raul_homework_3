package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class MainFrame {
	private JFrame frame;
	private JPanel panel1;
	private JLabel ID;
	private JLabel pass;
	private JTextField IDText;
	private JTextField passText;
	private JButton user;
	private JButton manager;
	private JButton create;
	private JButton delete;
	private Dimension dim;
	Font font1;
	Font font2;
	Color da;
	Color butoane;

	public MainFrame() {
		dim = Toolkit.getDefaultToolkit().getScreenSize();
		font1 = new Font("Verdana", Font.BOLD, 16);
		font2 = new Font("Verdana", Font.BOLD, 12);
		da = new Color(152, 251, 152);
		butoane = new Color(102, 205, 170);

		frame = new JFrame("Main Frame");

		ID = new JLabel("ID ", SwingConstants.CENTER);
		pass = new JLabel("PASS ", SwingConstants.CENTER);
		IDText = new JTextField();
		passText = new JTextField();
		user = new JButton("USER");
		manager = new JButton("MANAGER");
		create = new JButton("CREATE");
		delete = new JButton("DELETE");
		new JTextField();
		panel1 = new JPanel();

		frame();
		panel();
		labels();
		texts();
		buttons();

	}

	private void buttons() {
		user.setBounds(300, 20, 100, 29);
		user.setFont(font2);
		user.setBackground(butoane);
		panel1.add(user);

		manager.setBounds(300, 70, 100, 29);
		manager.setFont(font2);
		manager.setBackground(butoane);
		panel1.add(manager);

		create.setBounds(420, 20, 100, 29);
		create.setFont(font2);
		create.setBackground(butoane);
		panel1.add(create);

		delete.setBounds(420, 70, 100, 29);
		delete.setFont(font2);
		delete.setBackground(butoane);
		panel1.add(delete);

	}

	private void texts() {
		IDText.setBounds(120, 20, 150, 30);
		IDText.setFont(font2);
		IDText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(IDText);

		passText.setBounds(120, 70, 150, 30);
		passText.setFont(font2);
		passText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(passText);

	}

	private void labels() {
		ID.setBounds(50, 20, 60, 29);
		ID.setBackground(da);
		ID.setOpaque(true);
		ID.setFont(font1);
		panel1.add(ID);

		pass.setBounds(50, 70, 60, 29);
		pass.setBackground(da);
		pass.setOpaque(true);
		pass.setFont(font1);
		panel1.add(pass);

	}

	private void panel() {
		panel1.setBounds(0, 0, 580, 170);
		panel1.setLayout(null);
		panel1.setBackground(Color.white);
		frame.add(panel1);

	}

	public void frame() {
		frame.setBounds(0, 0, 580, 170);
		frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
		frame.setVisible(true);

	}

	public String getIDText() {
		return IDText.getText();
	}

	public void setIDText(String iDText) {
		IDText.setText(iDText);
	}

	public String getPassText() {
		return passText.getText();
	}

	public void setPassText(String passText) {
		this.passText.setText(passText);
		;
	}

	public JButton getUser() {
		return user;
	}

	public JButton getCreate() {
		return create;
	}

	public JButton getDelete() {
		return delete;
	}

	public JButton getManager() {
		return manager;
	}

}
