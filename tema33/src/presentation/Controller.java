package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import bll.ClientBll;
import bll.ManagerBll;
import bll.OrderBll;
import bll.ProductBll;
import bll.ShoppingBll;
import model.Bill;
import model.Client;
import model.Orders;
import model.Products;
import model.ShoppingCart;
import tables.TableClientDAO;
import tables.TableManagerDAO;
import tables.TableProductDAO;

public class Controller {

	private MainFrame mainFrame;
	private ClientFrame clientFrame;
	private StringBuilder istoric;
	private Eroare erroare;
	private TableProductDAO tables;
	private TableClientDAO tables1;
	private TableManagerDAO tables2;
	private int pret;
	private int cid;
	private int pid;
	private int oid;
	private Bill bill;
	private ManagerFrame managerFrame;
	private ClientBll cbll;
	private ProductBll pbll;
	private OrderBll obll;
	private ShoppingBll sbll;
	private ManagerBll mbll;

	public Controller() {

		mainFrame = new MainFrame();
		istoric = new StringBuilder();
		
		tables = new TableProductDAO();
		tables1 = new TableClientDAO();
		tables2 = new TableManagerDAO();
		pbll = new ProductBll();
		cbll = new ClientBll();
		obll = new OrderBll();
		sbll = new ShoppingBll();
		mbll = new ManagerBll();
		oid = obll.setOrderId();
		pid = pbll.setProductId();
		cid = cbll.setClientId();
		// MAIN FRAME CREATE NEW USER
		mfCreate();
		// MAIN FRAME DELETE A USER
		mfDelete();
		// USER FRAME
		clientFrame();
		// MANAGER
		managerF();
	}

	public void managerEdit() {
		managerFrame.getEdit().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pbll.setProduct(new Products(Integer.parseInt(managerFrame.getIdText()),
						Integer.parseInt(managerFrame.getPriceText()), managerFrame.getNameText(),
						Integer.parseInt(managerFrame.getNum())));
				pbll.getPd().edit1(pbll.getProduct());
				pbll.upgrade();
				managerFrame.getTable1().setModel(tables.buildTableModel(pbll.getProducts()));
				try {
					clientFrame.getTabel().setModel(tables.buildTableModel(pbll.getProducts()));
				} catch (Exception e) {
				}
				;
			}

		});
	}

	public void managerDel() {
		managerFrame.getDel().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					pbll.setProduct(pbll.findById(Integer.parseInt(managerFrame.getIdText())));
					managerFrame.setNameText(pbll.getProduct().getName());
					managerFrame.setPriceText(String.valueOf(pbll.getProduct().getPrice()));
					managerFrame.setNum(String.valueOf(pbll.getProduct().getNumber()));
					pbll.getPd().delete1(Integer.parseInt(managerFrame.getIdText()));
					pbll.upgrade();
					managerFrame.getTable1().setModel(tables.buildTableModel(pbll.getProducts()));
					try {
						clientFrame.getTabel().setModel(tables.buildTableModel(pbll.getProducts()));
					} catch (Exception e) {
					}
					;
				} catch (Exception e) {
					erroare = new Eroare();
					erroare.setEroarea("THERE IS NO SUCH A PRODUCT");
				}
			}
		});
	}

	public void manInfo() {
		managerFrame.setNamecText(mbll.getManager().getName());
		managerFrame.setEmailText(mbll.getManager().getEmail());
		managerFrame.setPhoneText(mbll.getManager().getPhone());
		managerFrame.setMidText(String.valueOf(mbll.getManager().getId()));
		managerFrame.setPassText(mbll.getManager().getPass());
		managerFrame.setIdText(String.valueOf(pid));
	}

	public void manProd() {
		managerFrame.getAddProducts().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					pbll.setProduct(new Products(pid, Integer.parseInt(managerFrame.getPriceText()),
							managerFrame.getNameText(), Integer.parseInt(managerFrame.getNum())));
					pbll.getPd().insert1(pbll.getProduct());
					pid++;
					pbll.upgrade();
					managerFrame.getTable1().setModel(tables.buildTableModel(pbll.getProducts()));
					try {
						clientFrame.getTabel().setModel(tables.buildTableModel(pbll.getProducts()));
					} catch (Exception e) {
					}
				} catch (Exception e) {
					erroare = new Eroare();
					erroare.setEroarea("PLEASE INPUT ALL THE REQUIRED FIELDS");
				}
			}
		});
	}
	
	public void clientEdit() {
		clientFrame.getEdit().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Client c = new Client(Integer.parseInt(clientFrame.getUIDText()), clientFrame.getPassText(),
						clientFrame.getNamecText(), clientFrame.getaddressText(), clientFrame.getEmailText());
				cbll.getCd().edit1(c);
				cbll.updateClients();
				try {
					managerFrame.getTabel().setModel(tables1.buildTableModel(cbll.getClients()));
				} catch (Exception e) {
				}
			}

		});
	}

	public void clientSetInfo() {
		clientFrame.getTabel1().setModel(tables2.buildTableModel(mbll.getManagers()));
		clientFrame.setUIDText(mainFrame.getIDText());
		clientFrame.setPassText(mainFrame.getPassText());
		clientFrame.setEmailText(cbll.getClient().getEmail());
		clientFrame.setaddressText(cbll.getClient().getAddress());
		clientFrame.setNamecText(cbll.getClient().getName());
	}

	public void clientAddProd() {
		clientFrame.getAddProducts().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					pbll.setProduct(pbll.findById(Integer.parseInt(clientFrame.getIDText())));
					
					if (pbll.getProduct().getNumber() - Integer.parseInt(clientFrame.getNum()) >= 0) {
						sbll.setShopping(new ShoppingCart(oid, Integer.parseInt(clientFrame.getIDText()),
								Integer.parseInt(clientFrame.getNum())));
						pret = pret + pbll.getProduct().getPrice() * sbll.getShopping().getAmount();
						clientFrame.setPriceText(String.valueOf(pret));
						clientFrame.setNameText(String.valueOf(pbll.getProduct().getName()));
						sbll.getSd().insert1(sbll.getShopping());
						sbll.getShoppings().add(sbll.getShopping());
						pbll.getProduct().setNumber(pbll.getProduct().getNumber()-sbll.getShopping().getAmount());
						pbll.getPd().edit1(pbll.getProduct());
					} else {
						erroare = new Eroare();
						erroare.setEroarea("UNDERSTOCK, ONLY " + pbll.getProduct().getNumber() + " "
								+ pbll.getProduct().getName() + " AVAILABLE");
					}
				} catch (Exception e) {
					erroare = new Eroare();
					erroare.setEroarea("PLEASE INPUT ALL REQUIRED FIELDS");
				}
			}
		});
	}
	public void clientBuy() {
		clientFrame.getBuy().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					oid++;
					bill = new Bill("ORDER " + String.valueOf(oid));
					if (sbll.getShoppings().size() > 0) {
						obll.setOrder(new Orders(oid, Integer.parseInt(clientFrame.getUIDText()),
								Integer.parseInt(clientFrame.getPriceText())));
						istoric.append("	Order " + oid + "\n");
						obll.getPd().insert1(obll.getOrder());
						pret = 0;
						istoric.append(clientFrame.getNamecText() + " bought ");
						for (ShoppingCart s : sbll.getShoppings()) {
							pbll.setProduct(pbll.findById(s.getProductId()));

							istoric.append("\n" + s.getAmount() + " " + pbll.getProduct().getName() + "   "
									+ pbll.getProduct().getPrice());		
						}
						istoric.append("\n");
						bill.addRecords(istoric + " TOTAL " + clientFrame.getPriceText());
						bill.addRecords((cbll.findById(obll.getOrder().getClientId()).getAddress()+"\n"));
						bill.closeFile();
						pbll.upgrade();
						transactionSucceful();
					
					} else {
						erroare = new Eroare();
						erroare.setEroarea("PLEASE ADD ITEMS TO YOUR CART");
					}
					System.out.println(sbll.getShoppings());
					

				} catch (Exception e) {
					erroare = new Eroare();
					erroare.setEroarea("PLEASE ADD ITEMS TO YOUR CART");
				}
			}
		});
	}
	public void mfDelete() {
		mainFrame.getDelete().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Client c = cbll.findById(Integer.parseInt(mainFrame.getIDText()));
					cbll.getCd().delete1(c.getId());
					cbll.updateClients();
					try {
						managerFrame.getTabel().setModel(tables1.buildTableModel(cbll.getClients()));
					} catch (Exception e) {
					}
					;
				} catch (NumberFormatException e) {
					erroare = new Eroare();
					erroare.setEroarea("PLEASE INPUT THE ID YOU WANT TO DELETE");
				}
			}
		});
	}

	public void managerF() {
		mainFrame.getManager().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (mbll.managerCheck(Integer.parseInt(mainFrame.getIDText()), mainFrame.getPassText()) != null) {
					managerFrame = new ManagerFrame();
					try {
						managerFrame.getConsola().setText(istoric.toString());
						managerFrame.getTabel().setModel(tables1.buildTableModel(cbll.getClients()));
						managerFrame.getTable1().setModel(tables.buildTableModel(pbll.getProducts()));
					} catch (Exception e) {
					}
					;
					// CHECK IF INPUT IS VALID
					manInfo();
					// MANAGER ADD PRODUCTS
					manProd();
					// MANAGER DELETE PRODUCTS
					managerDel();
					// MANAGER EDIT PRODUCTS
					managerEdit();
				} else {
					erroare = new Eroare();
					erroare.setEroarea("INVALID ID AND/OR PASSWORD");
				}
			}
		});
	}

	public void mfCreate() {
		mainFrame.getCreate().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Client client = new Client();
				clientFrame = new ClientFrame();
				cid++;
				clientFrame.setUIDText(String.valueOf(cid));
				clientFrame.getEdit().addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if (!clientFrame.getNamecText().equals("")) {
							client.setName(clientFrame.getNamecText());
							client.setId(cid);
							client.setName(clientFrame.getNamecText());
							client.setPass(clientFrame.getPassText());
							client.setEmail(clientFrame.getEmailText());
							client.setAddress(clientFrame.getaddressText());
							cbll.getCd().insert1(client);
							cbll.updateClients();
							erroare = new Eroare();
							erroare.setEroarea("PLEASE RELOG");
						} else {
							erroare = new Eroare();
							erroare.setEroarea("PLEASE INPUT ALL YOUR INFORMATION");
						}
						try {
							managerFrame.getTabel().setModel(tables1.buildTableModel(cbll.getClients()));
						} catch (Exception e) {
						}
						;
					}
				});
			}
		});
	}

	public void transactionSucceful() {
		sbll.getShoppings().removeAll(sbll.getShoppings());
		clientFrame.getTabel().setModel(tables.buildTableModel(pbll.getProducts()));
		try {
			managerFrame.getTable1().setModel(tables.buildTableModel(pbll.getProducts()));
			managerFrame.setConsola(istoric.toString());
		} catch (Exception e) {
		}
		erroare = new Eroare();
		erroare.setEroarea("TRANSACTION SUCCEFUL");
	}



	public void clientFrame() {
		mainFrame.getUser().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// CHECK FOR VALID ID
				if (cbll.clientCheck(Integer.parseInt(mainFrame.getIDText()), mainFrame.getPassText()) != null) {
					clientFrame = new ClientFrame();
					cbll.setClient(cbll.findById((Integer.parseInt(mainFrame.getIDText()))));
					try {
						clientFrame.getTabel().setModel(tables.buildTableModel(pbll.getProducts()));
					} catch (Exception e) {
					}
					// USER SET INFO
					clientSetInfo();
					// USER EDIT
					clientEdit();
					// USER ADD PROD TO CART
					clientAddProd();
					// USER ORDER
					clientBuy();

				} else {
					erroare = new Eroare();
					erroare.setEroarea("INVALID ID AND/OR PASSWORD");
				}
			}
		});
	}

}
