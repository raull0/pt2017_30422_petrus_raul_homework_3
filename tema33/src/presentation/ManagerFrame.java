package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class ManagerFrame {
	private JFrame frame;
	private JPanel panel1;
	private JLabel addProduct;
	private JLabel name;
	private JLabel ID;
	private JLabel storeInfo;
	private JLabel number;
	private JLabel price;
	private JTextField IDText;
	private JTextField nameText;
	private JTextField num;
	private JTextField priceText;
	private JButton clients;
	private JButton addProducts;
	private JButton edit;
	private JButton del;
	private JButton products;
	private JButton istoric;
	private JTextArea consola;
	private JLabel namec;
	private JLabel phone;
	private JLabel email;
	private JTextField namecText;
	private JTextField phoneText;
	private JTextField emailText;
	private JLabel storeInfo1;
	private JLabel pass;
	private JLabel infor;
	private JLabel MID;
	private JTextField passText;
	private JTextField midText;
	private JButton edit1;
	private JScrollPane sp;
	private JScrollPane sp1;
	private JTable tabel;
	private JTable tabel1;
	Font font;
	Font font1;
	Font font2;
	Color fbBlue;
	Color da;
	Color butoane;
	Dimension dim;

	public ManagerFrame() {
		font = new Font("Verdana", Font.BOLD, 20);
		font1 = new Font("Verdana", Font.BOLD, 16);
		font2 = new Font("Verdana", Font.BOLD, 12);
		fbBlue = new Color(60, 179, 113);
		da = new Color(152, 251, 152);
		butoane = new Color(102, 205, 170);
		frame = new JFrame("MANAGER");
		panel1 = new JPanel();
		storeInfo = new JLabel("MANAGER INFO ", SwingConstants.CENTER);
		addProduct = new JLabel("ADD PRODUCT", SwingConstants.CENTER);
		dim = Toolkit.getDefaultToolkit().getScreenSize();
		name = new JLabel("NAME", SwingConstants.CENTER);
		ID = new JLabel("PID ", SwingConstants.CENTER);
		storeInfo = new JLabel("CLIENTS", SwingConstants.CENTER);
		number = new JLabel("NUMBER ", SwingConstants.CENTER);
		price = new JLabel("PRICE", SwingConstants.CENTER);
		IDText = new JTextField();
		nameText = new JTextField();
		num = new JTextField();
		priceText = new JTextField();
		clients = new JButton("Clients");
		addProducts = new JButton("Add");
		edit = new JButton("Edit");
		del = new JButton("Del");
		products = new JButton("Products");
		istoric = new JButton("Istoric ");
		consola = new JTextArea();
		email = new JLabel("EMAIL", SwingConstants.CENTER);
		phone = new JLabel("PHONE", SwingConstants.CENTER);
		namec = new JLabel("NAME", SwingConstants.CENTER);
		emailText = new JTextField();
		namecText = new JTextField();
		phoneText = new JTextField();
		MID = new JLabel("MID", SwingConstants.CENTER);
		midText = new JTextField();
		pass = new JLabel("PASS", SwingConstants.CENTER);
		passText = new JTextField();
		edit1 = new JButton("Edit");
		infor = new JLabel("INFO", SwingConstants.CENTER);
		tabel = new JTable();
		sp = new JScrollPane();
		sp1 = new JScrollPane();
		storeInfo1 = new JLabel("PRODUCTS", SwingConstants.CENTER);
		tabel1 = new JTable();
		
		frame.setBounds(0, 0, 1324, 550);
		frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
		frame.setVisible(true);

		panel1.setBounds(0, 0, 1324, 550);
		panel1.setLayout(null);
		panel1.setBackground(Color.white);
		frame.add(panel1);

		addProduct.setFont(font);
		addProduct.setBounds(0, 0, 480, 30);
		addProduct.setBackground(fbBlue);
		addProduct.setOpaque(true);
		panel1.add(addProduct);

		ID.setBounds(30, 60, 90, 29);
		ID.setBackground(da);
		ID.setOpaque(true);
		ID.setFont(font1);
		panel1.add(ID);

		sp = new JScrollPane(tabel);
		sp.setBounds(480, 30, 534, 210);
		panel1.add(sp);
		tabel.setEnabled(false);

		sp1 = new JScrollPane(tabel1);
		sp1.setBounds(479, 270, 534, 280);
		panel1.add(sp1);
		tabel.setEnabled(false);

		infor.setFont(font);
		infor.setBounds(0, 240, 480, 30);
		infor.setBackground(fbBlue);
		infor.setOpaque(true);
		panel1.add(infor);

		IDText.setBounds(130, 60, 150, 30);
		IDText.setFont(font2);
		IDText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(IDText);

		name.setBounds(30, 100, 90, 29);
		name.setBackground(da);
		name.setOpaque(true);
		name.setFont(font1);
		panel1.add(name);

		nameText.setBounds(130, 100, 150, 30);
		nameText.setFont(font2);
		nameText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(nameText);

		number.setBounds(30, 140, 90, 30);
		number.setBackground(da);
		number.setOpaque(true);
		number.setFont(font1);
		panel1.add(number);

		num.setBounds(130, 140, 150, 30);
		num.setFont(font2);
		num.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(num);

		price.setBounds(30, 180, 90, 30);
		price.setBackground(da);
		price.setOpaque(true);
		price.setFont(font1);
		panel1.add(price);

		priceText.setBounds(130, 180, 150, 30);
		priceText.setFont(font2);
		priceText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(priceText);

		addProducts.setBounds(320, 80, 80, 29);
		addProducts.setFont(font1);
		addProducts.setBackground(butoane);
		panel1.add(addProducts);

		edit.setBounds(320, 120, 80, 29);
		edit.setFont(font1);
		edit.setBackground(butoane);
		panel1.add(edit);

		del.setBounds(320, 160, 80, 29);
		del.setFont(font1);
		del.setBackground(butoane);
		panel1.add(del);

		storeInfo.setFont(font);
		storeInfo.setBounds(480, 0, 534, 30);
		storeInfo.setBackground(fbBlue);
		storeInfo.setOpaque(true);
		panel1.add(storeInfo);

		storeInfo1.setFont(font);
		storeInfo1.setBounds(480, 240, 534, 30);
		storeInfo1.setBackground(fbBlue);
		storeInfo1.setOpaque(true);
		panel1.add(storeInfo1);

		consola.setBounds(1015, 0, 300, 550);
		consola.setFont(font2);
		consola.setBackground(Color.LIGHT_GRAY);
		consola.setAlignmentX(JTextArea.CENTER_ALIGNMENT);
		panel1.add(consola);

		MID.setBounds(30, 300, 90, 29);
		MID.setBackground(da);
		MID.setOpaque(true);
		MID.setFont(font1);
		panel1.add(MID);

		midText.setBounds(130, 300, 150, 30);
		midText.setFont(font2);
		midText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(midText);

		pass.setBounds(30, 340, 90, 29);
		pass.setBackground(da);
		pass.setOpaque(true);
		pass.setFont(font1);
		panel1.add(pass);

		passText.setBounds(130, 340, 150, 30);
		passText.setFont(font2);
		passText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(passText);

		email.setBounds(30, 460, 90, 29);
		email.setBackground(da);
		email.setOpaque(true);
		email.setFont(font1);
		panel1.add(email);

		emailText.setBounds(130, 460, 250, 30);
		emailText.setFont(font2);
		emailText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(emailText);

		namec.setBounds(30, 380, 90, 29);
		namec.setBackground(da);
		namec.setOpaque(true);
		namec.setFont(font1);
		panel1.add(namec);

		namecText.setBounds(130, 380, 250, 30);
		namecText.setFont(font2);
		namecText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(namecText);

		phone.setBounds(30, 420, 90, 29);
		phone.setBackground(da);
		phone.setOpaque(true);
		phone.setFont(font1);
		panel1.add(phone);

		edit1.setBounds(320, 320, 80, 29);
		edit1.setFont(font1);
		edit1.setBackground(butoane);
		panel1.add(edit1);

		phoneText.setBounds(130, 420, 250, 30);
		phoneText.setFont(font2);
		phoneText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(phoneText);
	}

	public String getIdText() {
		return this.IDText.getText();
	}

	public void setIdText(String iDText) {
		IDText.setText(iDText);
	}

	public String getNameText() {
		return nameText.getText();
	}

	public void setNameText(String nameText) {
		this.nameText.setText(nameText);
	}

	public String getNum() {
		return num.getText();
	}

	public void setNum(String num) {
		this.num.setText(num);
	}

	public String getPriceText() {
		return priceText.getText();
	}

	public void setPriceText(String priceText) {
		this.priceText.setText(priceText);
	}

	public JButton getClients() {
		return clients;
	}

	public void setClients(JButton clients) {
		this.clients = clients;
	}

	public JButton getAddProducts() {
		return addProducts;
	}

	public void setAddProducts(JButton addProducts) {
		this.addProducts = addProducts;
	}

	public JButton getEdit() {
		return edit;
	}

	public void setEdit(JButton edit) {
		this.edit = edit;
	}

	public JButton getDel() {
		return del;
	}

	public void setDel(JButton del) {
		this.del = del;
	}

	public JButton getProducts() {
		return products;
	}

	public void setProducts(JButton products) {
		this.products = products;
	}

	public JButton getIstoric() {
		return istoric;
	}

	public void setIstoric(JButton istoric) {
		this.istoric = istoric;
	}

	public JTextArea getConsola() {
		return consola;
	}

	public void setConsola(String string) {
		this.consola.setText(string);
	}

	public String getNamecText() {
		return namecText.getText();
	}

	public void setNamecText(String string) {
		this.namecText.setText(string);
	}

	public String getPhoneText() {
		return phoneText.getText();
	}

	public void setPhoneText(String phoneText) {
		this.phoneText.setText(phoneText);
	}

	public String getEmailText() {
		return emailText.getText();
	}

	public void setEmailText(String emailText) {
		this.emailText.setText(emailText);
	}

	public JTable getTabel() {
		return tabel;
	}

	public String getMidText() {
		return this.midText.getText();
	}

	public String getPassText() {
		return this.passText.getText();
	}

	public void setMidText(String s) {
		this.midText.setText(s);
	}

	public void setPassText(String s) {
		this.passText.setText(s);
	}

	public JTable getTable1() {
		return this.tabel1;
	}
}
