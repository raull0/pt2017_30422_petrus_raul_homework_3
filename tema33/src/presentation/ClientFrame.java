package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class ClientFrame {
	private JFrame frame;
	private JPanel panel1;
	private JLabel addProduct;
	private JLabel name;
	private JLabel ID;
	private JLabel storeInfo;
	private JLabel number;
	private JLabel price;
	private JLabel UID;
	private JLabel pass;
	private JLabel seeProducts;
	private JLabel namec;
	private JLabel address;
	private JLabel email;
	private JTextField IDText;
	private JTextField nameText;
	private JTextField UIDText;
	private JTextField passText;
	private JTextField num;
	private JTextField priceText;
	private JTextField namecText;
	private JTextField addressText;
	private JTextField emailText;
	private JButton addProducts;
	private JButton edit;
	private JTextArea consola;
	private JTable tabel;
	private JTable tabel1;
	private JButton buy;
	private JScrollPane sp;
	private JScrollPane sp1;
	private JLabel seeManagers;
	Dimension dim;

	public ClientFrame() {
		Font font = new Font("Verdana", Font.BOLD, 20);
		Font font1 = new Font("Verdana", Font.BOLD, 16);
		Font font2 = new Font("Verdana", Font.BOLD, 12);
		Color fbBlue = new Color(60, 179, 113);
		Color da = new Color(152, 251, 152);
		Color butoane = new Color(102, 205, 170);

		frame = new JFrame("CLIENT");
		panel1 = new JPanel();
		addProduct = new JLabel("ORDER INFO", SwingConstants.CENTER);
		dim = Toolkit.getDefaultToolkit().getScreenSize();
		name = new JLabel("NAME", SwingConstants.CENTER);
		ID = new JLabel("PID ", SwingConstants.CENTER);
		storeInfo = new JLabel("INFO", SwingConstants.CENTER);
		number = new JLabel("NUMBER ", SwingConstants.CENTER);
		price = new JLabel("PRICE", SwingConstants.CENTER);
		IDText = new JTextField();
		nameText = new JTextField();
		UIDText = new JTextField();
		passText = new JTextField();
		num = new JTextField();
		priceText = new JTextField();
		addProducts = new JButton("Add");
		consola = new JTextArea();
		edit = new JButton("Edit");
		UID = new JLabel("UID", SwingConstants.CENTER);
		pass = new JLabel("PASS", SwingConstants.CENTER);
		seeProducts = new JLabel("PRODUCTS", SwingConstants.CENTER);
		email = new JLabel("EMAIL", SwingConstants.CENTER);
		address = new JLabel("ADDRESS", SwingConstants.CENTER);
		namec = new JLabel("NAME", SwingConstants.CENTER);
		emailText = new JTextField();
		namecText = new JTextField();
		addressText = new JTextField();
		buy = new JButton("Buy");
		tabel = new JTable();
		tabel1 = new JTable();
		seeManagers = new JLabel("MANAGERS", SwingConstants.CENTER);

		frame.setBounds(0, 0, 1024, 550);
		frame.setVisible(true);
		frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
		panel1.setBounds(0, 0, 490, 720);
		panel1.setLayout(null);
		panel1.setBackground(Color.white);
		frame.add(panel1);

		addProduct.setFont(font);
		addProduct.setBounds(0, 0, 480, 30);
		addProduct.setBackground(fbBlue);
		addProduct.setOpaque(true);
		panel1.add(addProduct);

		ID.setBounds(30, 60, 90, 29);
		ID.setBackground(da);
		ID.setOpaque(true);
		ID.setFont(font1);
		panel1.add(ID);

		IDText.setBounds(130, 60, 150, 30);
		IDText.setFont(font2);

		IDText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(IDText);

		name.setBounds(30, 140, 90, 30);
		name.setBackground(da);
		name.setOpaque(true);
		name.setFont(font1);
		panel1.add(name);

		nameText.setBounds(130, 140, 150, 30);
		nameText.setFont(font2);
		nameText.setEditable(false);
		nameText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(nameText);

		number.setBounds(30, 100, 90, 29);
		number.setBackground(da);
		number.setOpaque(true);
		number.setFont(font1);
		panel1.add(number);

		num.setBounds(130, 100, 150, 30);
		num.setFont(font2);
		num.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(num);

		price.setBounds(30, 180, 90, 30);
		price.setBackground(da);
		price.setOpaque(true);
		price.setFont(font1);
		panel1.add(price);

		priceText.setBounds(130, 180, 150, 30);
		priceText.setFont(font2);
		priceText.setEditable(false);
		priceText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(priceText);

		addProducts.setBounds(320, 120, 80, 29);
		addProducts.setFont(font1);
		addProducts.setBackground(butoane);
		panel1.add(addProducts);

		buy.setBounds(320, 160, 80, 29);
		buy.setFont(font1);
		buy.setBackground(butoane);
		panel1.add(buy);

		edit.setBounds(320, 320, 80, 29);
		edit.setFont(font1);
		edit.setBackground(butoane);
		panel1.add(edit);

		storeInfo.setFont(font);
		storeInfo.setBounds(0, 240, 480, 30);
		storeInfo.setBackground(fbBlue);
		storeInfo.setOpaque(true);
		panel1.add(storeInfo);

		UID.setBounds(30, 300, 90, 29);
		UID.setBackground(da);
		UID.setOpaque(true);
		UID.setFont(font1);
		panel1.add(UID);

		UIDText.setBounds(130, 300, 150, 30);
		UIDText.setFont(font2);
		UIDText.setEditable(false);
		UIDText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(UIDText);

		pass.setBounds(30, 340, 90, 29);
		pass.setBackground(da);
		pass.setOpaque(true);
		pass.setFont(font1);
		panel1.add(pass);

		passText.setBounds(130, 340, 150, 30);
		passText.setFont(font2);
		passText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(passText);

		seeManagers.setFont(font);
		seeManagers.setBounds(480, 330, 534, 29);
		seeManagers.setBackground(fbBlue);
		seeManagers.setOpaque(true);
		panel1.add(seeManagers);

		seeProducts.setFont(font);
		seeProducts.setBounds(480, 0, 534, 29);
		seeProducts.setBackground(fbBlue);
		seeProducts.setOpaque(true);
		panel1.add(seeProducts);

		sp = new JScrollPane(tabel);
		sp.setBounds(480, 30, 534, 300);
		tabel.setFocusable(false);
		tabel.setColumnSelectionAllowed(false);
		tabel.setRowSelectionAllowed(false);
		panel1.add(sp);
		tabel.setEnabled(false);
		
		sp1 = new JScrollPane(tabel1);
		sp1.setBounds(480,360, 534, 150);
		panel1.add(sp1);
		tabel1.setEnabled(false);

		email.setBounds(30, 420, 90, 29);
		email.setBackground(da);
		email.setOpaque(true);
		email.setFont(font1);
		panel1.add(email);

		emailText.setBounds(130, 420, 250, 30);
		emailText.setFont(font2);
		emailText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(emailText);

		namec.setBounds(30, 380, 90, 29);
		namec.setBackground(da);
		namec.setOpaque(true);
		namec.setFont(font1);
		panel1.add(namec);

		namecText.setBounds(130, 380, 250, 30);
		namecText.setFont(font2);
		namecText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(namecText);

		address.setBounds(30, 460, 90, 29);
		address.setBackground(da);
		address.setOpaque(true);
		address.setFont(font1);
		panel1.add(address);

		addressText.setBounds(130, 460, 250, 30);
		addressText.setFont(font2);
		addressText.setHorizontalAlignment(JTextField.CENTER);
		panel1.add(addressText);

	}
	public String getIDText() {
		return IDText.getText();
	}

	public void setIDText(String iDText) {
		IDText.setText(iDText);
	}

	public String getNameText() {
		return nameText.getText();
	}

	public void setNameText(String nameText) {
		this.nameText.setText(nameText);
	}

	public String getUIDText() {
		return UIDText.getText();
	}

	public void setUIDText(String uIDText) {
		UIDText.setText(uIDText);
	}

	public String getPassText() {
		return passText.getText();
	}

	public void setPassText(String passText) {
		this.passText.setText(passText);
	}

	public String getNum() {
		return num.getText();
	}

	public void setNum(String num) {
		this.num.setText(num);
	}

	public String getPriceText() {
		return priceText.getText();
	}

	public void setPriceText(String priceText) {
		this.priceText.setText(priceText);
	}

	public JButton getAddProducts() {
		return addProducts;
	}

	public JButton getEdit() {
		return edit;
	}

	public String getConsola() {
		return consola.getText();
	}

	public void setConsola(String consola) {
		this.consola.setText(consola);
	}

	public String getNamecText() {
		return namecText.getText();
	}

	public void setNamecText(String namecText) {
		this.namecText.setText(namecText);
	}

	public String getaddressText() {
		return addressText.getText();
	}

	public void setaddressText(String addressText) {
		this.addressText.setText(addressText);
	}

	public String getEmailText() {
		return emailText.getText();
	}

	public void setEmailText(String emailText) {
		this.emailText.setText(emailText);
	}

	public JButton getBuy() {
		return this.buy;
	}

	public JTable getTabel() {
		return tabel;
	}
	public JTable getTabel1() {
		return tabel1;
	}
	public void setTabel(JTable tabel) {
		this.tabel = tabel;
	}

	public void setTabel1(JTable tabel) {
		this.tabel1 = tabel;
	}

	public JLabel getSeeManagers() {
		return this.seeManagers;
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
