package model;

public class Manager {
	private int id;
	private String pass;
	private String name;
	private String phone;
	private String email;
	public Manager(int id, String pass, String name, String phone, String email){
		this.id = id ;
		this.pass= pass;
		this.name=name;
		this.phone=phone;
		this.email=email;
		
	}public Manager(){
		this.id = 0 ;
		this.pass= "";
		this.name="";
		this.phone="";
		this.email="";
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "Manager [id=" + id + ", pass=" + pass + ", name=" + name + ", phone=" + phone + ", email=" + email
				+ "]";
	}

}
