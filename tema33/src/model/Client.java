package model;

public class Client {
	private int id;
	private String pass;
	private String name;
	private String address;
	private String email;
	public Client() {
		this.id = 0;
		this.pass = "";
		this.name = "";
		this.address = "";
		this.email = "";
	}
	public Client(int id , String pass,String name, String address,String email){
		this.id = id ;
		this.pass=pass;
		this.name=name;
		this.address=address;
		this.email=email;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "Client [id=" + id + ", pass=" + pass + ", name=" + name + ", address=" + address + ", email=" + email
				+ "]";
	}
}
