package model;

import java.io.IOException;
import java.io.PrintWriter;

public class Bill {

	PrintWriter writer;

	public Bill(String s) {
		try {
			PrintWriter writer = new PrintWriter(s, "UTF-8");
			this.writer = writer;
		} catch (IOException e) {
			// do something
		}

	}

	public void addRecords(String s) {

		writer.println(s);

	}

	public void closeFile() {
		writer.close();
	}
}
