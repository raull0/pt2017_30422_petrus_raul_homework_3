package model;

public class Orders {

	private int id;
	private int clientId;
	private int price;

	public Orders() {
		this.id = 0;
		this.clientId = 0;
		this.price = 0;
	}

	public Orders(int id, int clientId, int price) {
		this.id = id;
		this.clientId = clientId;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", clientId=" + clientId + ", ="  + ", price=" + price + "]";
	}
}
