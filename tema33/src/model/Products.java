package model;

public class Products {
	private int id;
	private String name;
	private int number;
	private int price;
	public Products() {
		this.id = 0;
		this.price = 0;
		this.name = "";
		this.number=0;

	}
	public Products(int id, int price, String name,int number) {
		this.id = id;
		this.price = price;
		this.name = name;
		this.number = number;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Products [id=" + id + ", price=" + price + ", name=" + name + "]";
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}

}
