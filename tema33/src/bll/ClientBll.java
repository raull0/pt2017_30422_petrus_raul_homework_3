package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.ClientDAO;
import model.Client;

public class ClientBll {
	private Client client;
	private List<Client> clients;
	private ClientDAO cd;

	public ClientBll() {
		cd = new ClientDAO();
		clients = cd.findAll();
	}

	public Client findById(int id) {
		client = cd.findById(id);
		if (client == null) {
			throw new NoSuchElementException("The client with id =" + id + " was not found!");
		} else
			return client;
	}

	public Client clientCheck(int id, String pass) {
		boolean d = false;
		try {
			client = findById(id);
			if (id == client.getId() && (pass.equals(client.getPass()))) {
				d = true;
			}
		} catch (Exception e) {
		}
		if (d)
			return client;
		else
			return null;
	}
	public int setClientId(){
		if (clients.size() > 0)
			return clients.get(clients.size() - 1).getId() + 1;
		else
			return 1;
	}
	public void updateClients(){
		clients= cd.findAll();
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public ClientDAO getCd() {
		return cd;
	}

	public void setCd(ClientDAO cd) {
		this.cd = cd;
	}
}
