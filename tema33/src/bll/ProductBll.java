package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.ProductDAO;
import model.Products;

public class ProductBll {

	private Products product;
	private List<Products> products;
	private ProductDAO pd;

	public ProductBll() {
		pd = new ProductDAO();
		products = pd.findAll();
		product = new Products();
	}

	public Products findById(int id) {

		this.product = pd.findById(id);
		if (product == null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		} else
			return product;
	}

	public int setProductId() {
		if (products.size() > 0)
			return products.get(products.size() - 1).getId() + 1;
		else
			return 1;
	}

	public void insert1() {

	}

	public void upgrade() {
		products = pd.findAll();
	}

	public Products getProduct() {
		return product;
	}

	public void setProduct(Products product) {

		this.product = product;

	}

	public List<Products> getProducts() {
		return products;
	}

	public void setProducts(List<Products> products) {
		this.products = products;
	}

	public ProductDAO getPd() {
		return pd;
	}

	public void setPd(ProductDAO pd) {
		this.pd = pd;
	}

}
