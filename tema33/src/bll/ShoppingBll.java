package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import dao.ShoppingCartDAO;
import model.ShoppingCart;

public class ShoppingBll {

	private ShoppingCart shopping;
	private List<ShoppingCart> shoppings;
	private ShoppingCartDAO sd;

	public ShoppingBll() {
		sd = new ShoppingCartDAO();
		shoppings = new ArrayList<ShoppingCart>();
	}

	public ShoppingCart findById(int id) {
		shopping = sd.findById(id);
		if (shopping == null) {
			throw new NoSuchElementException("The Shopping with id =" + id + " was not found!");
		} else
			return shopping;
	}

	public ShoppingCart getShopping() {
		return shopping;
	}

	public void setShopping(ShoppingCart shopping) {
		this.shopping = shopping;
	}

	public List<ShoppingCart> getShoppings() {
		return shoppings;
	}

	public void setShoppings(List<ShoppingCart> shoppings) {
		this.shoppings = shoppings;
	}

	public ShoppingCartDAO getSd() {
		return sd;
	}

	public void setSd(ShoppingCartDAO sd) {
		this.sd = sd;
	}

}
