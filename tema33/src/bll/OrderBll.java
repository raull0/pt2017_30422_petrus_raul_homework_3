package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.OrderDAO;
import model.Orders;

public class OrderBll {

	private Orders order;
	private List<Orders> orders;
	private OrderDAO od;

	public OrderBll() {
		od = new OrderDAO();
		orders = od.findAll();
	}

	public Orders findById(int id) {
		order = od.findById(id);
		if (order == null) {
			throw new NoSuchElementException("The Order with id =" + id + " was not found!");
		} else
			return order;
	}
	public int setOrderId(){
		if (orders.size() > 0)
			return  orders.get(orders.size() - 1).getId() + 1;
		else	
		
			return  1;
	
	}
	public void upgrade(){
		orders = od.findAll();
	}

	public Orders getOrder() {
		return order;
	}

	public void setOrder(Orders order) {
		this.order = order;
	}

	public List<Orders> getOrders() {
		return orders;
	}

	public void setOrders(List<Orders> orders) {
		this.orders = orders;
	}

	public OrderDAO getPd() {
		return od;
	}

	public void setPd(OrderDAO od) {
		this.od =od;
	}

}