package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.ManagerDAO;
import model.Manager;

public class ManagerBll {
	private Manager manager;
	private List<Manager> managers;
	private ManagerDAO md;

	public ManagerBll() {
		md = new ManagerDAO();
		managers = md.findAll();
	}

	public Manager findById(int id) {
		manager = md.findById(id);
		if (manager == null) {
			throw new NoSuchElementException("The manager with id =" + id + " was not found!");
		} else
			return manager;
	}

	public Manager managerCheck(int id, String pass) {
		boolean d = false;
		try {
			manager = findById(id);
			if (id == manager.getId() && (pass.equals(manager.getPass()))) {
				d = true;
			}
		} catch (Exception e) {

		}
		if (d)
			return manager;
		else
			return null;

	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	public List<Manager> getManagers() {
		return managers;
	}

	public void setManagers(List<Manager> managers) {
		this.managers = managers;
	}

	public ManagerDAO getMd() {
		return md;
	}

	public void setMd(ManagerDAO md) {
		this.md = md;
	}
}
