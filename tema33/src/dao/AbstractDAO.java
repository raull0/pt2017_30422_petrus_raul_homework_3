package dao;

import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;

import connection.ConnectionFactory;

public class AbstractDAO<T> {

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}

	/*
	 * Creates a string containing a SQL instruction to find a tuple by a field(
	 * usually ID)
	 */
	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName().toLowerCase());
		sb.append(" WHERE " + field + " = ?");
		return sb.toString();
	}

	/*
	 * Finds a tuple by ID
	 */
	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);// precompiles the
															// statement
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
		} finally {
			ConnectionFactory.close(connection);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(resultSet);

		}
		return null;
	}

	/*
	 * Creates a string containing a SQL query to find all tuples in a relation
	 */
	private String queryFindAll() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		return sb.toString();
	}

	public List<T> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = queryFindAll();
		connection = ConnectionFactory.getConnection();
		try {
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();
			return createObjects(resultSet);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(connection);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(resultSet);

		}
		return null;
	}

	private List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (Exception e) {
		}

		return list;
	}

	public String insert(T t) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append(t.getClass().getSimpleName().toLowerCase());
		sb.append(" (");
		try {
			for (Field field : type.getDeclaredFields()) {
				sb.append(field.getName());
				sb.append(", ");
			}
			sb.deleteCharAt(sb.length() - 2);
			sb.deleteCharAt(sb.length() - 1);
			sb.append(") VALUES (");
			for (Field field : type.getDeclaredFields()) {
				String s = field.getType().toString();
				PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
				Method method = propertyDescriptor.getReadMethod();
				if (s.equals("int")) {
					sb.append(method.invoke(t).toString() + ",");
				} else {
					sb.append("'" + method.invoke(t).toString());
					sb.append("',");
				}
			}
			sb.deleteCharAt(sb.length() - 1);
			sb.append(");");
		} catch (Exception e) {
		}
		return sb.toString();
	}

	public void insert1(T t) {
		Connection connection = null;
		PreparedStatement statement = null;
		connection = ConnectionFactory.getConnection();
		String query = insert(t);

		try {
			statement = connection.prepareStatement(query);
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(connection);
			ConnectionFactory.close(statement);

		}

	}

	/*
	 * Creates a string containing a SQL instruction to delete a tuple by ID
	 */
	public String delete(String id) {
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE ");
		sb.append("FROM ");
		sb.append(type.getSimpleName().toLowerCase());
		sb.append(" WHERE " + id + " = ?");
		return sb.toString();
	}

	/*
	 * Applies the instruction sent by delete method
	 */
	public void delete1(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		connection = ConnectionFactory.getConnection();
		String query = delete("id");
		try {

			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (Exception e) {

		} finally

		{
			ConnectionFactory.close(connection);
			ConnectionFactory.close(statement);

		}

	}

	/*
	 * Creates a string containing a SQL instruction to edit a
	 * client/order/product by id
	 */
	public String edit(T t) {
		int a = 0;
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE " + type.getSimpleName().toLowerCase() + " SET ");
		try {
			for (Field field : type.getDeclaredFields()) {
				sb.append(field.getName() + "= ");
				String s = field.getType().toString();
				PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
				Method method = propertyDescriptor.getReadMethod();
				if (s.equals("int")) {
					sb.append(method.invoke(t).toString() + ",");
				} else {
					sb.append("'" + method.invoke(t).toString());
					sb.append("',");
				}
				if (field.getName() == "id")
					a = (int) method.invoke(t);
			}
		} catch (Exception e) {
		}
		;
		sb.deleteCharAt(sb.length() - 1);
		sb.append(" WHERE id = " + a);
		return sb.toString();
	}

	/*
	 * Applies the instruction sent by edit method
	 */
	public void edit1(T t) {
		Connection connection = null;
		PreparedStatement statement = null;
		connection = ConnectionFactory.getConnection();
		String query = edit(t);

		try {

			statement = connection.prepareStatement(query);
			statement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(connection);
			ConnectionFactory.close(statement);

		}

	}

}